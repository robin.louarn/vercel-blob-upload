"use server";

import { head } from "@vercel/blob";

export const getMetadata = async (url: string) => head(url);
